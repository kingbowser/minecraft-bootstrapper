package net.minecraft.bootstrap;

import java.util.List;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import net.minecraft.hopper.HopperService;
import java.io.PrintStream;
import java.io.ByteArrayOutputStream;
import java.net.Authenticator;
import java.net.SocketAddress;
import java.net.InetSocketAddress;
import joptsimple.OptionException;
import joptsimple.OptionParser;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.lang.reflect.Constructor;
import java.net.URLClassLoader;
import java.net.URL;
import javax.swing.text.Document;
import javax.swing.SwingUtilities;
import javax.swing.JScrollBar;
import javax.swing.text.BadLocationException;
import javax.swing.text.AttributeSet;
import java.math.BigInteger;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.io.IOException;
import java.util.jar.Pack200;
import java.io.OutputStream;
import java.util.jar.JarOutputStream;
import java.io.Closeable;
import java.io.FileOutputStream;
import java.io.InputStream;
import LZMA.LzmaInputStream;
import java.io.FileInputStream;
import java.util.concurrent.TimeUnit;
import java.util.Date;
import java.text.DateFormat;
import java.util.Locale;
import javax.swing.border.Border;
import java.awt.Component;
import javax.swing.text.DefaultCaret;
import java.net.PasswordAuthentication;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.net.Proxy;
import java.io.File;
import java.awt.Font;
import javax.swing.JFrame;

public class Bootstrap extends JFrame
{
    private static final Font MONOSPACED;
    public static final String LAUNCHER_URL = "https://s3.amazonaws.com/Minecraft.Download/launcher/launcher.pack.lzma";
    private final File workDir;
    private final Proxy proxy;
    private final File launcherJar;
    private final File packedLauncherJar;
    private final File packedLauncherJarNew;
    private final JTextArea textArea;
    private final JScrollPane scrollPane;
    private final PasswordAuthentication proxyAuth;
    private final String[] remainderArgs;
    private final StringBuilder outputBuffer;
    
    public Bootstrap(final File workDir, final Proxy proxy, final PasswordAuthentication proxyAuth, final String[] remainderArgs) {
        super("Minecraft Launcher");
        this.outputBuffer = new StringBuilder();
        this.workDir = workDir;
        this.proxy = proxy;
        this.proxyAuth = proxyAuth;
        this.remainderArgs = remainderArgs;
        this.launcherJar = new File(workDir, "launcher.jar");
        this.packedLauncherJar = new File(workDir, "launcher.pack.lzma");
        this.packedLauncherJarNew = new File(workDir, "launcher.pack.lzma.new");
        this.setSize(854, 480);
        this.setDefaultCloseOperation(3);
        (this.textArea = new JTextArea()).setLineWrap(true);
        this.textArea.setEditable(false);
        this.textArea.setFont(Bootstrap.MONOSPACED);
        ((DefaultCaret)this.textArea.getCaret()).setUpdatePolicy(1);
        (this.scrollPane = new JScrollPane(this.textArea)).setBorder(null);
        this.scrollPane.setVerticalScrollBarPolicy(22);
        this.add(this.scrollPane);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.println("Bootstrap (v5)");
        this.println("Current time is " + DateFormat.getDateTimeInstance(2, 2, Locale.US).format(new Date()));
        this.println("System.getProperty('os.name') == '" + System.getProperty("os.name") + "'");
        this.println("System.getProperty('os.version') == '" + System.getProperty("os.version") + "'");
        this.println("System.getProperty('os.arch') == '" + System.getProperty("os.arch") + "'");
        this.println("System.getProperty('java.version') == '" + System.getProperty("java.version") + "'");
        this.println("System.getProperty('java.vendor') == '" + System.getProperty("java.vendor") + "'");
        this.println("System.getProperty('sun.arch.data.model') == '" + System.getProperty("sun.arch.data.model") + "'");
        this.println("");
    }
    
    public void execute(final boolean force) {
        if (this.packedLauncherJarNew.isFile()) {
            this.println("Found cached update");
            this.renameNew();
        }
        final Downloader.Controller controller = new Downloader.Controller();
        if (force || !this.packedLauncherJar.exists()) {
            final Downloader downloader = new Downloader(controller, this, this.proxy, null, this.packedLauncherJarNew);
            downloader.run();
            if (controller.hasDownloadedLatch.getCount() != 0L) {
                throw new FatalBootstrapError("Unable to download while being forced");
            }
            this.renameNew();
        }
        else {
            final String md5 = this.getMd5(this.packedLauncherJar);
            final Thread thread = new Thread(new Downloader(controller, this, this.proxy, md5, this.packedLauncherJarNew));
            thread.setName("Launcher downloader");
            thread.start();
            try {
                this.println("Looking for update");
                final boolean wasInTime = controller.foundUpdateLatch.await(3L, TimeUnit.SECONDS);
                if (controller.foundUpdate.get()) {
                    this.println("Found update in time, waiting to download");
                    controller.hasDownloadedLatch.await();
                    this.renameNew();
                }
                else if (!wasInTime) {
                    this.println("Didn't find an update in time.");
                }
            }
            catch (InterruptedException e) {
                throw new FatalBootstrapError("Got interrupted: " + e.toString());
            }
        }
        this.unpack();
        this.startLauncher(this.launcherJar);
    }
    
    public void unpack() {
        final File lzmaUnpacked = this.getUnpackedLzmaFile(this.packedLauncherJar);
        InputStream inputHandle = null;
        OutputStream outputHandle = null;
        this.println("Reversing LZMA on " + this.packedLauncherJar + " to " + lzmaUnpacked);
        try {
            inputHandle = new LzmaInputStream(new FileInputStream(this.packedLauncherJar));
            outputHandle = new FileOutputStream(lzmaUnpacked);
            final byte[] buffer = new byte[65536];
            for (int read = inputHandle.read(buffer); read >= 1; read = inputHandle.read(buffer)) {
                outputHandle.write(buffer, 0, read);
            }
        }
        catch (Exception e) {
            throw new FatalBootstrapError("Unable to un-lzma: " + e);
        }
        finally {
            closeSilently(inputHandle);
            closeSilently(outputHandle);
        }
        this.println("Unpacking " + lzmaUnpacked + " to " + this.launcherJar);
        JarOutputStream jarOutputStream = null;
        try {
            jarOutputStream = new JarOutputStream(new FileOutputStream(this.launcherJar));
            Pack200.newUnpacker().unpack(lzmaUnpacked, jarOutputStream);
        }
        catch (Exception e2) {
            throw new FatalBootstrapError("Unable to un-pack200: " + e2);
        }
        finally {
            closeSilently(jarOutputStream);
        }
        this.println("Cleaning up " + lzmaUnpacked);
        lzmaUnpacked.delete();
    }
    
    public static void closeSilently(final Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            }
            catch (IOException ex) {}
        }
    }
    
    private File getUnpackedLzmaFile(final File packedLauncherJar) {
        String filePath = packedLauncherJar.getAbsolutePath();
        if (filePath.endsWith(".lzma")) {
            filePath = filePath.substring(0, filePath.length() - 5);
        }
        return new File(filePath);
    }
    
    public String getMd5(final File file) {
        DigestInputStream stream = null;
        try {
            stream = new DigestInputStream(new FileInputStream(file), MessageDigest.getInstance("MD5"));
            final byte[] buffer = new byte[65536];
            for (int read = stream.read(buffer); read >= 1; read = stream.read(buffer)) {}
        }
        catch (Exception ignored) {
            return null;
        }
        finally {
            closeSilently(stream);
        }
        return String.format("%1$032x", new BigInteger(1, stream.getMessageDigest().digest()));
    }
    
    public void println(final String string) {
        this.print(string + "\n");
    }
    
    public void print(final String string) {
        System.out.print(string);
        this.outputBuffer.append(string);
        final Document document = this.textArea.getDocument();
        final JScrollBar scrollBar = this.scrollPane.getVerticalScrollBar();
        final boolean shouldScroll = scrollBar.getValue() + scrollBar.getSize().getHeight() + Bootstrap.MONOSPACED.getSize() * 2 > scrollBar.getMaximum();
        try {
            document.insertString(document.getLength(), string, null);
        }
        catch (BadLocationException ex) {}
        if (shouldScroll) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    scrollBar.setValue(Integer.MAX_VALUE);
                }
            });
        }
    }
    
    public void startLauncher(final File launcherJar) {
        this.println("Starting launcher.");
        try {
            final Class<?> aClass = new URLClassLoader(new URL[] { launcherJar.toURI().toURL() }).loadClass("net.minecraft.launcher.Launcher");
            final Constructor<?> constructor = aClass.getConstructor(JFrame.class, File.class, Proxy.class, PasswordAuthentication.class, String[].class, Integer.class);
            constructor.newInstance(this, this.workDir, this.proxy, this.proxyAuth, this.remainderArgs, 5);
        }
        catch (Exception e) {
            throw new FatalBootstrapError("Unable to start: " + e);
        }
    }
    
    public void renameNew() {
        if (this.packedLauncherJar.exists() && !this.packedLauncherJar.isFile() && !this.packedLauncherJar.delete()) {
            throw new FatalBootstrapError("while renaming, target path: " + this.packedLauncherJar.getAbsolutePath() + " is not a file and we failed to delete it");
        }
        if (this.packedLauncherJarNew.isFile()) {
            this.println("Renaming " + this.packedLauncherJarNew.getAbsolutePath() + " to " + this.packedLauncherJar.getAbsolutePath());
            if (this.packedLauncherJarNew.renameTo(this.packedLauncherJar)) {
                this.println("Renamed successfully.");
            }
            else {
                if (this.packedLauncherJar.exists() && !this.packedLauncherJar.canWrite()) {
                    throw new FatalBootstrapError("unable to rename: target" + this.packedLauncherJar.getAbsolutePath() + " not writable");
                }
                this.println("Unable to rename - could be on another filesystem, trying copy & delete.");
                if (this.packedLauncherJarNew.exists() && this.packedLauncherJarNew.isFile()) {
                    try {
                        copyFile(this.packedLauncherJarNew, this.packedLauncherJar);
                        if (this.packedLauncherJarNew.delete()) {
                            this.println("Copy & delete succeeded.");
                        }
                        else {
                            this.println("Unable to remove " + this.packedLauncherJarNew.getAbsolutePath() + " after copy.");
                        }
                        return;
                    }
                    catch (IOException e) {
                        throw new FatalBootstrapError("unable to copy:" + e);
                    }
                }
                this.println("Nevermind... file vanished?");
            }
        }
    }
    
    public static void copyFile(final File source, final File target) throws IOException {
        if (!target.exists()) {
            target.createNewFile();
        }
        FileChannel sourceChannel = null;
        FileChannel targetChannel = null;
        try {
            sourceChannel = new FileInputStream(source).getChannel();
            targetChannel = new FileOutputStream(target).getChannel();
            targetChannel.transferFrom(sourceChannel, 0L, sourceChannel.size());
        }
        finally {
            if (sourceChannel != null) {
                sourceChannel.close();
            }
            if (targetChannel != null) {
                targetChannel.close();
            }
        }
    }
    
    public static void main(final String[] args) throws IOException {
        System.setProperty("java.net.preferIPv4Stack", "true");
        final OptionParser optionParser = new OptionParser();
        optionParser.allowsUnrecognizedOptions();
        optionParser.accepts("help", "Show help").forHelp();
        optionParser.accepts("force", "Force updating");
        final OptionSpec<String> proxyHostOption = optionParser.accepts("proxyHost", "Optional").withRequiredArg();
        final OptionSpec<Integer> proxyPortOption = optionParser.accepts("proxyPort", "Optional").withRequiredArg().defaultsTo("8080", new String[0]).ofType(Integer.class);
        final OptionSpec<String> proxyUserOption = optionParser.accepts("proxyUser", "Optional").withRequiredArg();
        final OptionSpec<String> proxyPassOption = optionParser.accepts("proxyPass", "Optional").withRequiredArg();
        final OptionSpec<File> workingDirectoryOption = optionParser.accepts("workDir", "Optional").withRequiredArg().ofType(File.class).defaultsTo(Util.getWorkingDirectory(), new File[0]);
        final OptionSpec<String> nonOptions = optionParser.nonOptions();
        OptionSet optionSet;
        try {
            optionSet = optionParser.parse(args);
        }
        catch (OptionException e) {
            optionParser.printHelpOn(System.out);
            System.out.println("(to pass in arguments to minecraft directly use: '--' followed by your arguments");
            return;
        }
        if (optionSet.has("help")) {
            optionParser.printHelpOn(System.out);
            return;
        }
        final String hostName = optionSet.valueOf(proxyHostOption);
        Proxy proxy = Proxy.NO_PROXY;
        if (hostName != null) {
            try {
                proxy = new Proxy(Proxy.Type.SOCKS, new InetSocketAddress(hostName, optionSet.valueOf(proxyPortOption)));
            }
            catch (Exception ex) {}
        }
        final String proxyUser = optionSet.valueOf(proxyUserOption);
        final String proxyPass = optionSet.valueOf(proxyPassOption);
        PasswordAuthentication passwordAuthentication = null;
        if (!proxy.equals(Proxy.NO_PROXY) && stringHasValue(proxyUser) && stringHasValue(proxyPass)) {
            final PasswordAuthentication auth;
            passwordAuthentication = (auth = new PasswordAuthentication(proxyUser, proxyPass.toCharArray()));
            Authenticator.setDefault(new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return auth;
                }
            });
        }
        final File workingDirectory = optionSet.valueOf(workingDirectoryOption);
        if (workingDirectory.exists() && !workingDirectory.isDirectory()) {
            throw new FatalBootstrapError("Invalid working directory: " + workingDirectory);
        }
        if (!workingDirectory.exists() && !workingDirectory.mkdirs()) {
            throw new FatalBootstrapError("Unable to create directory: " + workingDirectory);
        }
        final List<String> strings = optionSet.valuesOf(nonOptions);
        final String[] remainderArgs = strings.toArray(new String[strings.size()]);
        final boolean force = optionSet.has("force");
        final Bootstrap frame = new Bootstrap(workingDirectory, proxy, passwordAuthentication, remainderArgs);
        try {
            frame.execute(force);
        }
        catch (Throwable t) {
            final ByteArrayOutputStream stracktrace = new ByteArrayOutputStream();
            t.printStackTrace(new PrintStream(stracktrace));
            final StringBuilder report = new StringBuilder();
            report.append(stracktrace).append("\n\n-- Head --\nStacktrace:\n").append(stracktrace).append("\n\n").append((CharSequence)frame.outputBuffer);
            report.append("\tMinecraft.Bootstrap Version: 5");
            try {
                HopperService.submitReport(proxy, report.toString(), "Minecraft.Bootstrap", "5");
            }
            catch (Throwable t2) {}
            frame.println("FATAL ERROR: " + stracktrace.toString());
            frame.println("\nPlease fix the error and restart.");
        }
    }
    
    public static boolean stringHasValue(final String string) {
        return string != null && !string.isEmpty();
    }
    
    static {
        MONOSPACED = new Font("Monospaced", 0, 12);
    }
}
