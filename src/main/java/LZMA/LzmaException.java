package LZMA;

import java.io.*;

public class LzmaException extends IOException
{
    private static final long serialVersionUID = 3689351022372206390L;
    
    public LzmaException() {
    }
    
    public LzmaException(final String s) {
        super(s);
    }
}
