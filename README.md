MC Launcher Bootstrap
=====================

This is the fully functioning and compilable source code
for the minecraft launcher's bootstrapper.

The bootstrapper downloads, decompresses, caches, and executes
the actual minecraft launcher.

In order to compile a fully functional jar, simply execute `gradle fatjar`.

The compiled jar can be found in `build/libs`
